﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class StepCounter : MonoBehaviour
{
    public Text mStepsText;
    private int mStepNumber = 0;

    private void Start()
    {
        SetSteps();
    }

    private void SetSteps()
    {
        int sceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (sceneIndex == 1)
        {
            mStepNumber = 11;
        }
        else if (sceneIndex == 2)
        {
            mStepNumber = 13;
        }
        else if (sceneIndex == 3 || sceneIndex == 4 || sceneIndex == 6)
        {
            mStepNumber = 14;
        }
        else if (sceneIndex == 5)
        {
            mStepNumber = 15;
        }
        else if (sceneIndex == 7)
        {
            mStepNumber = 25;
        }
        else if (sceneIndex == 8)
        {
            mStepNumber = 16;
        }
        else if (sceneIndex == 9)
        {
            mStepNumber = 27;
        }
        mStepsText.text = mStepNumber.ToString();
    }

    public void RemoveStep()
    {
        mStepNumber = int.Parse(mStepsText.text);
        if (mStepNumber == 0)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        else
        {
            mStepNumber--;
            mStepsText.text = mStepNumber.ToString();
        }
    }
}
