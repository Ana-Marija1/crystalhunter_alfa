﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
    public GameObject mPauseMenuUI;
    public GameObject mLevel;

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
                mPauseMenuUI.SetActive(true);
                mLevel.SetActive(false);
            
        }
    }

    private void LoadLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    private void ExitGame()
    {
        Debug.Log("QUIT!");
        Application.Quit();
    }
}
