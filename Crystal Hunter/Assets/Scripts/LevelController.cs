﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelController : MonoBehaviour
{
    private void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("Player"))
        {
            if (gameObject.CompareTag("Crystal"))
            {
                gameObject.GetComponent<Renderer>().enabled = false;
            }
            if(SceneManager.GetActiveScene().buildIndex != 9)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
            }
            //Goes back to main menu
            else
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}
